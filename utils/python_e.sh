#!/usr/bin/env bash

python_script="""
import sys

def main():
    for x in sys.stdin:
        try:
            x = x.strip()
            $1
        except BrokenPipeError:
            break


if __name__ == '__main__':
    main()
"""

python -c "$python_script"
