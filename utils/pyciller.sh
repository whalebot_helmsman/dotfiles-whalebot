#!/usr/bin/env bash

DIR=$1

if [ -z "$DIR" ];
then
    DIR='./'
fi

find $1 -name '*.pyc' -print0 | xargs -0 rm -rf
find $1 -name '__pycache__' -print0 | xargs -0 rm -rf
