#!/usr/bin/env bash
set -e

venv=$1
if [ -z $venv ]; then
    echo "usage: runinenv [virtualenv_path] CMDS"
    exit 1
fi

possible_dirs=(
                Scripts
                bin
              )

function env_dir {
    echo "${venv}/${1}/activate"
}

for dir in "${possible_dirs[@]}"
do
    activate_file=`env_dir $dir`
    if [ -f "$activate_file" ];
    then
        break
    fi
done

activate_file=`env_dir $dir`
. ${activate_file}
shift 1
echo "Executing $@ in ${venv}" >&2
exec "$@"
deactivate
