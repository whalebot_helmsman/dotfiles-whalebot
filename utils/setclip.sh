test_existance()
{
   command -v "$1" > /dev/null
   echo "$?"
}

is_xorg=`test_existance xclip`
is_wayland=`test_existance wl-copy`
if [[ "$is_xorg" == "0" ]]
then
    xclip -selection c
elif [[ "$is_wayland" == "0" ]]
then
    wl-copy
else
    echo "no clipboard utility found" >&2
fi
