ex_dir() {
    if [ -z "$EXPERIMENT_ROOT" ];
    then
        echo "EXPERIMENT_ROOT is not set, nothing is done" >&2
        return
    fi

    local moment=$1
    if [ -z "$moment" ];
    then
        moment=`date '+%Y-%m-%d'`
    fi
    EXDIR=$EXPERIMENT_ROOT/$moment
    mkdir -p $EXDIR
    echo "EXDIR=${EXDIR}" >&2
    export EXDIR
}
