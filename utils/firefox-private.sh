#!/bin/sh -e
DIR="${XDG_CACHE_HOME:-$HOME/.cache}"
mkdir -p -- "$DIR"
TEMP="$(mktemp -d -- "$DIR/firefox-XXXXXX")"
trap "rm -rf -- '$TEMP'" INT TERM EXIT
user_js=$TEMP/user.js
wget \
    'https://raw.githubusercontent.com/pyllyukko/user.js/master/user.js' \
    --output-document="$user_js" \
    --timeout=20

if [ "$?" != "0" ];
then
    script=`which $0`
    cp `dirname $script`/user.js "$user_js"
fi

firefox -profile "$TEMP" -no-remote "$@"

