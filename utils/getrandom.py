#!/usr/bin/env python
import sys
import random


def main():
    upper_limit =   int(sys.argv[1])

    if len(sys.argv) > 2:
        lower_limit =   int(sys.argv[2])
    else:
        lower_limit =   0

    result  =   random.randint(lower_limit, upper_limit)

    print result

if __name__ == "__main__":
    main()
