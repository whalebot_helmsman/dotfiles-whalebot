#!/usr/bin/env bash
# We still need this.
windows() { [[ -n "$WINDIR" ]]; }

# Cross-platform symlink function. With one parameter, it will check
# whether the parameter is a symlink. With two parameters, it will create
# a symlink to a file or directory, with syntax: link $linkname $target
make_link() {
    # Link-creation mode.
    if windows; then
        linkname=`cygpath -wa "$1"`
        target=`cygpath -wa "$2"`
        # Windows needs to be told if it's a directory or not. Infer that.
        # Also: note that we convert `/` to `\`. In this case it's necessary.
        if [[ -d "$2" ]]; then
            cmd <<< "mklink /D \"${linkname}\" \"${target}\"" > /dev/null
        else
            cmd <<< "mklink \"${linkname}\" \"${target}\"" > /dev/null
        fi
    else
        # You know what? I think ln's parameters are backwards.
        ln -s "$2" "$1"
    fi
}

PARENT_DIR=$1
if [ -z "$PARENT_DIR" ];
then
    PARENT_DIR=~/
fi
pushd "$PARENT_DIR" > /dev/null
PARENT_DIR=`pwd`
popd > /dev/null

TARGET_DIR=$2
if [ -z "$TARGET_DIR" ];
then
    TARGET_DIR=`dirname "$0"`/..
fi

pushd "$TARGET_DIR" > /dev/null
TARGET_DIR=`pwd`
popd > /dev/null

function link_to {
    local SOURCE_FILE=$1
    local LINK=$2
    local CHMOD=$3
    local SOURCE_FILE_FULL=$TARGET_DIR/$SOURCE_FILE
    local LINK_FULL=$PARENT_DIR/$LINK

    if [ ! -e "$SOURCE_FILE_FULL" ];
    then
        return
    fi

    LINK_LINKED_TO=`readlink "$LINK_FULL" || true`

    if [ "$LINK_LINKED_TO" = "$SOURCE_FILE_FULL" ];
    then
        if [ -n "$CHMOD" ];
        then
            chmod "$CHMOD" "$LINK_FULL"
        fi
        return
    fi

    if [ -e "$LINK_FULL" ];
    then
        LINK_FULL_MOVE=${LINK_FULL}.old
        echo "moving \"$LINK_FULL\" to \"$LINK_FULL_MOVE\""
        mv "$LINK_FULL" "$LINK_FULL_MOVE"
    fi

    LINK_DIR=`dirname $LINK`
    mkdir -p $LINK_DIR
    pushd "$PARENT_DIR" > /dev/null
    make_link "$LINK" "$SOURCE_FILE_FULL"
    if [ -n "$CHMOD" ];
    then
        chmod "$CHMOD" "$LINK"
    fi
    popd > /dev/null
}

TASKS='
git/.gitconfig	.gitconfig
vim/.vim	.vim
vim/.vimrc	.vimrc
vim/.gvimrc	.gvimrc
hg/.hgrc	.hgrc
ssh/config	.ssh/config	600
bash/.inputrc	.inputrc
bash/.bash_profile	.bash_profile
pip/pip.conf	.pip/pip.conf
tmux/.tmux.conf	.tmux.conf
tmux/.tmux	.tmux
'

echo "$TASKS" |
while read -r TASK;
do
    if [ -z "$TASK" ];
    then
        continue
    fi

    SOURCE=`echo "$TASK" | cut -f 1`
    LINKNAME=`echo "$TASK" | cut -f 2`
    CHMOD=`echo "$TASK" | cut -f 3`

    link_to "$SOURCE" "$LINKNAME" "$CHMOD"
done
