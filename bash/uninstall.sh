rm -rf "${HOME}/.gdbinit" \
       "${HOME}/.vim" \
       "${HOME}/.vimrc" \
       "${HOME}/.gvimrc" \
       "${HOME}/.subversion/config" \
       "${HOME}/.hgrc" \
       "${HOME}/.ssh/config" \
       "${HOME}/.pentadactylrc" \
       "${HOME}/.inputrc" \
       "${HOME}/.bash_profile" \
       "${HOME}/.tmux.conf" \
       "${HOME}/.pip/pip.conf"
