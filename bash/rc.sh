#!/usr/bin/env bash

PARENT_DIR=$1
if [ -z "$PARENT_DIR" ];
then
    PARENT_DIR=~/
fi
pushd "$PARENT_DIR" > /dev/null
PARENT_DIR=`pwd`
popd > /dev/null

TARGET_DIR_NAME=dotfiles-whalebot
TARGET_DIR=$PARENT_DIR/$TARGET_DIR_NAME
pushd "$TARGET_DIR" > /dev/null
TARGET_DIR=`pwd`
popd > /dev/null

BASH_DIR=$TARGET_DIR/bash
source "$BASH_DIR/updater.sh" "$PARENT_DIR" "$TARGET_DIR"

QUIRKS="$BASH_DIR/quirks.sh"
if [ -e "$QUIRKS" ]
then
    source "$QUIRKS"
fi

export DOTFILES_DIR=$TARGET_DIR

PS1='\n\[\033[G\][\[\e[0;32m\]\u\[\e[m\]@\[\e[1;34m\]\h:\w\[\e[m\]]\[\e[1;32m\]\$\[\e[m\] \[\e[1;37m\]\[\e[0m\]'

export LC_CTYPE=en_US.UTF-8
export LC_COLLATE=POSIX

HISTFILESIZE=1000000000
HISTSIZE=1000000
shopt -s histappend

SCRIPTS="
$TARGET_DIR/make/hmake
$TARGET_DIR/make/highlight-gcc.py
$TARGET_DIR/utils/ack
$TARGET_DIR/utils/pyciller.sh
$TARGET_DIR/utils/getrandom.py
$TARGET_DIR/utils/setclip.sh
$TARGET_DIR/utils/getclip.sh
"

for SCRIPT in $SCRIPTS;
do
    chmod u+x "$SCRIPT"
done

ADD_TO_PATH="
$HOME/.local/bin
"

function add_to_path {
    local ADDER=$1

    if [[ "$ADDER" == */ ]];
    then
        local LEN=${#ADDER}
        local LEN=$((LEN - 1))
        ADDER="${ADDER:0:$LEN}"
    fi

    if [[ ":$PATH:" != *":$ADDER:"* ]] && [[ ":$PATH:" != *":$ADDER/:"* ]];
    then
        PATH="$ADDER:$PATH"
    fi
}

for SCRIPT in $SCRIPTS;
do
    add_to_path `dirname "$SCRIPT"`
done

for ADDER in $ADD_TO_PATH;
do
    add_to_path "$ADDER"
done

# support for color output in GCC>=4.9
# does not work in ninja
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

SOURCED=(
    $TARGET_DIR/utils/experiment_dir.sh
)

for provider in "${SOURCED[@]}"
do
  source $provider
done
