
if exists("loaded_stripline_plugin")
  finish
endif

let loaded_comments_plugin="v0.01"

function! StripLines()
    let l:current_line_content = getline(".")
    let l:save_cursor = getpos(".")
    %s/\s\+$//e
    call setpos('.', l:save_cursor)
    call setline(".", l:current_line_content)
    call setpos('.', l:save_cursor)
endfunction
