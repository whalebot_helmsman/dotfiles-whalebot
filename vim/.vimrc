filetype off
syntax off
call pathogen#infect()

scriptencoding utf-8
set nocp

"file presets
set ff=unix
set encoding=utf-8                                  " set charset translation encoding
set termencoding=utf-8                              " set terminal encoding
set fileencoding=utf-8                              " set save encoding
set fileencodings=utf8,cp1251,koi8r,cp866,ucs-2le   " список предполагаемых кодировок, в порядке предпочтения
filetype plugin indent on

"backup off
set nobackup
set noswapfile

"chrome
set t_Co=256
colorscheme mustang
set cul
set list!
set listchars=tab:>>,trail:⋅
set guicursor=a:blinkon0

if v:version >= 703
    set rnu
else
    set nu
endif

set sc
set scrolloff=3
syntax on
set synmaxcol=200

"statusline
if has("statusline")
 set statusline=%<%f\ %y%h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
 set statusline +=
endif
set laststatus=2

"keyboard handling
set backspace=indent,eol,start

"tab/spaces
set smartindent
set expandtab
set tabstop=4
set shiftwidth=4

"search
set incsearch
set hlsearch
set ignorecase
set smartcase
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

"remove trailing spaces
autocmd BufWritePre * :call StripLines()

"line length delimiter
if exists('+colorcolumn')
      set colorcolumn=80
else
    :highlight MyGroup ctermbg=grey guibg=grey
    au BufWinEnter * let w:m2=matchadd('MyGroup', '\%80v', -1)
endif

if exists("+showtabline")
    if !exists("MyTabLine_function_loaded")
        let MyTabLine_function_loaded = "1"
        function MyTabLine()
          let s = '' " complete tabline goes here
          " loop through each tab page
          for t in range(tabpagenr('$'))
            " select the highlighting for the buffer names
            if t + 1 == tabpagenr()
              let s .= '%#TabLineSel#'
            else
              let s .= '%#TabLine#'
            endif
            " empty space
            let s .= ' '
            " set the tab page number (for mouse clicks)
            let s .= '%' . (t + 1) . 'T'
            " set page number string
            let s .= t + 1 . ' '
            " get buffer names and statuses
            let n = ''  "temp string for buffer names while we loop and check buftype
            let m = 0 " &modified counter
            let bc = len(tabpagebuflist(t + 1))  "counter to avoid last ' '
            " loop through each buffer in a tab
            for b in tabpagebuflist(t + 1)
              " buffer types: quickfix gets a [Q], help gets [H]{base fname}
              " others get 1dir/2dir/3dir/fname shortened to 1/2/3/fname
              if getbufvar( b, "&buftype" ) == 'help'
                let n .= '[H]' . fnamemodify( bufname(b), ':t:s/.txt$//' )
              elseif getbufvar( b, "&buftype" ) == 'quickfix'
                let n .= '[Q]'
              else
                let n .= pathshorten(bufname(b))
                "let n .= bufname(b)
              endif
              " check and ++ tab's &modified count
              if getbufvar( b, "&modified" )
                let m += 1
              endif
              " no final ' ' added...formatting looks better done later
              if bc > 1
                let n .= ' '
              endif
              let bc -= 1
            endfor
            " add modified label [n+] where n pages in tab are modified
            if m > 0
              "let s .= '[' . m . '+]'
              let s.= '+ '
            endif
            " add buffer names
            if n == ''
              let s .= '[No Name]'
            else
              let s .= n
            endif
            " switch to no underlining and add final space to buffer list
            "let s .= '%#TabLineSel#' . ' '
            let s .= ' '
          endfor
          " after the last tab fill with TabLineFill and reset tab page nr
          let s .= '%#TabLineFill#%T'
          " right-align the label to close the current tab page
          if tabpagenr('$') > 1
            let s .= '%=%#TabLine#%999XX'
          endif
          return s
        endfunction
    endif

    set tabline=%!MyTabLine()
endif

"cyrrilic letter mappings
if v:version >= 702
  set langmap=йqцwуeкrеtнyгuшiщoзpх[ъ]фaыsвdаfпgрhоjлkдlж\\;э'
           \яzчxсcмvиbтnьmб\\,ю.ЙQЦWУEКRЕTНYГUШIЩOЗPХ{Ъ}
           \ФAЫSВDАFПGРHОJЛKДLЖ:Э\\"ЯZЧXСCМVИBТNЬMБ<Ю>ё`Ё~№#
endif

"syntastic checkers
let loaded_cpp_syntax_checker = 1

"only correct autopair
let g:AutoPairs = {'{':'}'}

"split handling
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

if &history < 1000
  set history=1000
endif

set viminfo+=:1000

let g:rainbow_active = 1
let g:syntastic_python_checkers = ['pyflakes']

autocmd BufRead,BufNewFile *.md setlocal spell
autocmd BufRead,BufNewFile *.rst setlocal spell

let g:python_highlight_all = 1
